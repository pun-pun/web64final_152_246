-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 03, 2022 at 03:23 PM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fruitshopping`
--

-- --------------------------------------------------------

--
-- Table structure for table `Buyer`
--

CREATE TABLE `Buyer` (
  `BuyerId` int(11) NOT NULL,
  `BuyerName` varchar(50) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Buyer`
--

INSERT INTO `Buyer` (`BuyerId`, `BuyerName`, `Username`, `Password`, `IsAdmin`) VALUES
(1, 'tanatat', 'tanatat', '123456', 0),
(2, 'guy', '', '', 0),
(3, 'aou', '', '', 0),
(4, 'tu', 'tu.n', '123456', 0),
(5, 'pass', 'pass.a', '$2b$10$Gkol2o6Cn8R1F1xfXiI/wusaBYs4C3xY4/kw7dtNlD6QcVDAXIi.6', 0),
(6, 'som', 'undefined', 'undefined', 0),
(7, 'kanom', 'kanom.t', '$2b$10$mDKYOuWvDAtiEjml2XgRyu3u8vVKTGAM6iJfooGYymSxlIFD9GBt6', 0),
(8, 'kanom2', 'kanom2.t', '$2b$10$/hb7KSeIZVZe9ZDRviN8bOC0eJlwEIn346fKy0rhikzYbfXvjJlOe', 0),
(9, 'undefined', 'undefined', '$2b$10$zA91Vp8oNNzzHQoJR4toROh6oEKdVxYXqPKZdw3SOdu40AIZMbKBi', 0),
(10, 'kanom2', 'kanom2.t', '$2b$10$2aUlqwVGuQKOPwWbKmVcpOV3wYwkjf0bww5kEb/qF0.GndIeS2Jgq', 0),
(11, 'ton', 'ton.t', '$2b$10$8VqUW95rgK9h5.XyD39xLubH44DCMPhxVPSGgoUIeqIkrOHl3yMo.', 0),
(12, 'charthai', 'charthai.a', '$2b$10$HPn61pCmfW4dKwaz5s1zC..N2SzlEeC3.xGVc6aTbtDb.t5PM8GE2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Buying`
--

CREATE TABLE `Buying` (
  `BuyingId` int(11) NOT NULL,
  `buyerId` int(11) NOT NULL,
  `FruitId` int(11) NOT NULL,
  `Unit` varchar(11) NOT NULL,
  `BuyingTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Buying`
--

INSERT INTO `Buying` (`BuyingId`, `buyerId`, `FruitId`, `Unit`, `BuyingTime`) VALUES
(1, 1, 1, '5', '2022-04-03 11:19:07'),
(2, 2, 2, '5', '2022-04-03 11:19:07'),
(3, 1, 2, 'undefined', '2022-04-03 11:48:24'),
(4, 5, 3, '5', '2022-04-03 13:43:23'),
(5, 11, 2, '5', '2022-04-03 15:10:00'),
(6, 11, 5, '5', '2022-04-03 15:15:47'),
(7, 12, 5, '5', '2022-04-03 15:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `Fruit`
--

CREATE TABLE `Fruit` (
  `FruitId` int(11) NOT NULL,
  `FruitName` varchar(100) NOT NULL,
  `FruitResult` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Fruit`
--

INSERT INTO `Fruit` (`FruitId`, `FruitName`, `FruitResult`) VALUES
(1, 'มะม่วง', 'มะม่วงสุกผลสีเหลือง'),
(2, 'ส้ม', 'ส้มผลสีส้ม'),
(3, 'มังคุด', 'มังคุดสีม่วง'),
(4, 'ทุเรียน', 'ทุเรียนอร่อย'),
(5, 'แตงโม', 'แตงโมเนื้อสีแดง');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Buyer`
--
ALTER TABLE `Buyer`
  ADD PRIMARY KEY (`BuyerId`);

--
-- Indexes for table `Buying`
--
ALTER TABLE `Buying`
  ADD PRIMARY KEY (`BuyingId`),
  ADD KEY `BuyerId` (`buyerId`),
  ADD KEY `FruitId` (`FruitId`);

--
-- Indexes for table `Fruit`
--
ALTER TABLE `Fruit`
  ADD PRIMARY KEY (`FruitId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Buyer`
--
ALTER TABLE `Buyer`
  MODIFY `BuyerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `Buying`
--
ALTER TABLE `Buying`
  MODIFY `BuyingId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `Fruit`
--
ALTER TABLE `Fruit`
  MODIFY `FruitId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Buying`
--
ALTER TABLE `Buying`
  ADD CONSTRAINT `BuyerId` FOREIGN KEY (`buyerId`) REFERENCES `Buyer` (`BuyerId`),
  ADD CONSTRAINT `FruitId` FOREIGN KEY (`FruitId`) REFERENCES `Fruit` (`FruitId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
