import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

function BuyItem (props) {

        return (
        
            <Box sx={{ flexGrow: 1 }}>
            <Card sx={{width:"100%"}}>
                <CardMedia
                    component="img"
                    height="300px"
                    image={props.imageitem}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {props.itemname}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {props.detail}
                    </Typography>
                </CardContent>
                <CardActions>
                    {props.cost} &nbsp;
                    <Button color="info" variant="contained" size="small">ชื้อ</Button> 
                </CardActions>
            </Card>
           </Box>
        );
    }
    
    export default BuyItem;