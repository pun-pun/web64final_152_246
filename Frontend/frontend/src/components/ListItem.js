import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

function createData(
    itemname: string,
    detail: string,
    cost: number,
  ) {
    return { itemname, cost, detail };
  }
  
  const rows = [
    createData('มะม่วง', 'จ.พะเยา', 300),
    createData('มังคุด', 'จ.ลำปาง', 250),
    createData('ลำไย', 'จ.นครพนม', 410),
    createData('เงาะ', 'จ.ลำพูน', 120),
    createData('องุ่น', 'จ.พะเยา', 310),
  ];

function ListItem (props) {

    return (
    
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                <TableRow>
                    <TableCell>ชื่อสินค้า</TableCell>
                    <TableCell align="right">รายละเอียด</TableCell>
                    <TableCell align="right">ราคา</TableCell>
                    <TableCell align="right">&nbsp;</TableCell>
                    <TableCell align="right">&nbsp;</TableCell>

                </TableRow>
                </TableHead>
                <TableBody>
                {rows.map((props) => (
                    <TableRow
                    key={props.itemname}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                    <TableCell component="th" scope="row">
                        {props.itemname}
                    </TableCell>
                    <TableCell align="right">
                        {props.detail}</TableCell>
                    <TableCell align="right">
                        {props.cost}</TableCell>
                    <TableCell align="center">
                    <Button color="info" size="small">รายละเอียด</Button></TableCell> 
                    
                    <TableCell align="center">
                    <Button color="info" variant="outlined" size="small">ชื้อ</Button></TableCell>

                    </TableRow>
                ))}
                </TableBody>
            </Table>
        </TableContainer>

        
    );
}

export default ListItem;