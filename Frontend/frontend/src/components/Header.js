import { Link } from "react-router-dom";

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

import IconButton from '@mui/material/IconButton';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import AddBusinessIcon from '@mui/icons-material/AddBusiness';
import LoginIcon from '@mui/icons-material/Login';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';

function Header() {
    return(

    <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h7">
                    แอพซื้อขายสินค้าเกษตร : 
                </Typography>

                &nbsp;&nbsp;
                <Link to="/">
                    <Typography  variant="body1">
                        <Button color="info" variant="contained" >
                            <IconButton color="primary" aria-label="add to shopping cart" size="small">
                                <AddBusinessIcon  fontSize="inherit" />
                            </IconButton>
                        เลือกซื้อสินค้า</Button>
                    </Typography>
                </Link>

                &nbsp;&nbsp;
                <Link to="sell">
                    <Typography variant="body1">
                        <Button color="info" variant="contained">
                            <IconButton color="primary" aria-label="add to shopping cart" size="small">
                                <AddShoppingCartIcon fontSize="inherit" />
                            </IconButton>
                        ลงขายสินค้า
                        </Button>
                    </Typography>
                </Link>

                &nbsp;&nbsp;
                <Link to="login">
                    <Typography variant="body1">
                        <Button color="info" variant="contained">
                            <IconButton color="primary" aria-label="add to shopping cart" size="small">
                                <LoginIcon fontSize="inherit" />
                            </IconButton>
                        เข้าสู่ระบบ
                        </Button>
                    </Typography>
                </Link>
                
                &nbsp;&nbsp;
                <Link to="register">
                    <Typography variant="body1">
                        <Button color="secondary" variant="contained">
                            <IconButton color="primary" aria-label="add to shopping cart" size="small">
                                <AppRegistrationIcon fontSize="small" />
                            </IconButton>
                        ลงทะเบียน
                        </Button>
                    </Typography>
                </Link>
                
            </Toolbar>
        </AppBar>
    </Box>     
      
    );
}

export default Header; 