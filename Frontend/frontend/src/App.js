import './App.css';

import Header from './components/Header';

import BuyItemPage from './pages/BuyItemPage';
import SellItemPage from './pages/SellItemPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';

import Footer from './components/Footer';
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path="sell"  element={
          <SellItemPage />
          }/>

          <Route path="/" element={
          <BuyItemPage />
          } /> 

          <Route path="login" element={
          <LoginPage />
          } /> 

          <Route path="register" element={
          <RegisterPage />
          } /> 

      </Routes>
      <Footer />
    </div>
  );
}

export default App;
