import { useState } from "react";

import Button from '@mui/material/Button';
import Box from '@mui/material/Box';

function SellItemPage() {

    const [ itemname, setItemname ] = useState();
    const [ cost, setCost ] = useState();
    const [ detail, setDetail ] = useState();

    return (
        <div align="left">
            <div align="left" margin="20px">
                <h2 align="left">&nbsp;&nbsp;&nbsp;ลงขายสินค้า</h2>
                <hr /><br /><br />
                &nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Box component="span" sx={{ p: 4, border: '1px dashed grey' }}>
                    <Button>ใส่รูปภาพ</Button>
                </Box><br /><br /><br />
                &nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;
                ชื่อสินค้า
                   &nbsp;
                   &nbsp; : <input type="text"
                              value={ itemname} 
                              onChange={ (e) => {setItemname(e.target.value);} }/> <br />
                <br />
                &nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;
                ราคา
                   &nbsp;&nbsp;
                   &nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp; : <input type="text"
                              value={ cost } 
                              onChange={ (e) => {setCost(e.target.value);} }/> <br />
                <br />
                &nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;              
                รายละเอียด : <input type="text"
                              value={ detail } 
                              onChange={ (e) => {setDetail(e.target.value);} }/> <br />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;
                <Button color="info" variant="contained">ตกลง</Button>
                <br /><br /><br /><br /><br /><br /><br /><br />
            </div>
        </div>
    );
}

export default SellItemPage;