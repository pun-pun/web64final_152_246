import BuyItem from "../components/BuyItem";
import ListItem from "../components/ListItem";

function BuyItemPage() {
    return (
        <div>
            
                <h2 align="left">&nbsp;&nbsp;รายการสินค้าแนะนำ</h2>
                <hr />
                <BuyItem 
                         itemname="ส้ม"
                         imageitem="..\orange.jpg"
                         cost="200 บาท"
                         detail="ส้มสดๆจาก จ.นครปฐม" />

                <hr />
                <h2 align="left">&nbsp;&nbsp;รายการสินค้า</h2>
                <hr />
                <ListItem />
        </div>
    );
}

export default BuyItemPage;