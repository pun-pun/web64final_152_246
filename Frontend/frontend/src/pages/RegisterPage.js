import { useState } from "react";

import Button from '@mui/material/Button';

function RegisterPage() {

    const [ name, setName ] = useState();
    const [ id, setID ] = useState();
    const [ password, setPassword ] = useState();

    return (
        <div align="left">
            <div align="center">
                <h2>Register</h2>
                <hr /><br /><br />
                NAME
                   &nbsp;&nbsp;
                   &nbsp; : <input type="text"
                              value={ name} 
                              onChange={ (e) => {setName(e.target.value);} }/> <br />
                <br />
                ID &nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp; : <input type="text"
                              value={ id } 
                              onChange={ (e) => {setID(e.target.value);} }/> <br />
                <br />              
                Password : <input type="text"
                              value={ password } 
                              onChange={ (e) => {setPassword(e.target.value);} }/> <br />
                <br />
                <Button color="secondary" variant="contained">ลงทะเบียน</Button>
                <br /><br /><br /><br /><br /><br /><br /><br />

            </div>
        </div>
    );
}

export default RegisterPage;