import { useState } from "react";

import Button from '@mui/material/Button';

function LoginPage() {

    const [ id, setID ] = useState();
    const [ password, setPassword ] = useState();

    return (
        <div align="left">
            <div align="center">
                <h2>Login</h2>
                <hr /><br /><br />

                ID &nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp; : <input type="text"
                              value={ id } 
                              onChange={ (e) => {setID(e.target.value);} }/> <br />
                <br />              
                Password : <input type="text"
                              value={ password } 
                              onChange={ (e) => {setPassword(e.target.value);} }/> <br />
                <br />
                <Button color="info" variant="contained">เข้าสู่ระบบ</Button>
                <br /><br /><br /><br /><br /><br /><br /><br />

            </div>
        </div>
    );
}

export default LoginPage;