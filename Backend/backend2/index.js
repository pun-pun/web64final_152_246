const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET


const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user: 'admin1',
    password: 'admin1',
    database: 'fruitshopping'
});

connection.connect();

const express = require('express');
const app = express()
const port = 4000

/* Middleware for Authenticating User Token */
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) {return res.sendStatus(403) }
        else{
            req.user = user
            next()
        }
    })
}


/* list cart */
app.get("/list_cart", (req, res) => {

    let query = "SELECT * from Buying";
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            res.json(rows)
        }
    });
});

/* API for Registering to a new buying  */
app.post("/register_cart", authenticateToken, (req, res) => {
    let user_profile = req.user
    let fruit_id = req.query.fruit_id
    let buyer_id = req.user.user_id
    let unit = req.query.unit
    

    let query = `INSERT INTO Buying 
                (FruitId, BuyerId, Unit, BuyingTime) 
                VALUES ('${fruit_id}','${buyer_id}','${unit}',NOW() )`
    console.log(query)
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "success" 
            })
        }
    });
})

/* API for Processing User Autorizayion */
app.post("/login", (req, res) => {
    let username = req.query.username
    let buyer_password = req.query.password
    let query = `SELECT * FROM Buyer WHERE Username='${username}'`
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(buyer_password, db_password, (err, result)=> {
                if(result) {
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].BuyerId,
                        "IsAdmin" : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '7200s'})
                    res.send(token)
                }else { res.send("Invalid username / password")}
            })
        }
    })
})


/* API for Registering a new buyer */

app.post("/register_buyer", (req, res) => {

    let buyer_name = req.query.buyer_name
    let buyer_username = req.query.buyer_username
    let buyer_password = req.query.buyer_password
    bcrypt.hash(buyer_password, SALT_ROUNDS, (err, hash) => {
            let query = `INSERT INTO Buyer 
                    (BuyerName, Username, Password, IsAdmin) 
                    VALUES ('${buyer_name}','${buyer_username}','${hash}', false)`
        console.log(query)

        connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "success" 
        })
        }
        });
    })
});
app.post("/delete_buyer", (req, res) => {

    let buyer_id = req.query.buyer_id
    
    let query = `DELETE FROM Buyer WHERE BuyerId=${buyer_id}`
    
    console.log(query)
        
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({
                    "status" : "400",
                    "message" : "Error deleting data into db" 
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "deleting success" 
        })
        }
    });
    
});

/* list of Fruit */
app.get("/list_fruit", (req, res) =>{
    let query = "SELECT * from Fruit";
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            res.json(rows)
        }
    });
});

/* add Fruit */
app.post("/add_fruit", (req, res) => {
    let fruit_name = req.query.fruit_name
    let fruit_result = req.query.fruit_result

    let query = `INSERT INTO Fruit 
                (FruitName, FruitResult) 
                VALUES ('${fruit_name}', '${fruit_result}')`
    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error" 
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "success" 
        })
        }
    });

});

/* DELETE Fruit */
app.post("/delete_fruit", (req, res) => {

    let fruit_id = req.query.fruit_id

    let query = `DELETE FROM Fruit WHERE FruitId=${fruit_id}`

    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error deleting data into db" 
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "deleting success" 
        })
        }
    });

});



app.listen(port, () => {
    console.log(`Now starting Running System backend ${port} `)

})